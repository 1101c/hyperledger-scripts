#!/bin/bash
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Usage:
#
# ./prereqs-ubuntu.sh
#
# User must then logout and login upon completion of script
#

# Exit on any failure
set -e

# Install HyperLedger Fabric

echo "# Installing Hyperledger Binaries"
mkdir ~/FabricBinaries && cd ~/FabricBinaries
curl -sSL https://goo.gl/6wtTN5 | bash -s 1.1.0
echo 'export PATH=~/FabricBinaries/bin:$PATH' >>~/.profile
source ~/.profile
cd ..
git clone https://1101c@bitbucket.org/1101c/fabric-samples.git
echo "# Installation is complete." 
